package com.amap.navi.demo.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.amap.api.navi.AMapNaviView;
import com.amap.api.navi.AMapNaviViewOptions;
import com.amap.api.navi.enums.NaviType;
import com.amap.api.navi.model.AMapCalcRouteResult;
import com.amap.api.navi.model.NaviLatLng;
import com.amap.navi.demo.R;

public class SingleRouteCalculateActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_basic_navi);

        initWayPointList();
        mAMapNaviView = (AMapNaviView) findViewById(R.id.navi_view);
        mAMapNaviView.onCreate(savedInstanceState);
        mAMapNaviView.setAMapNaviViewListener(this);
//        mAMapNaviView.setVisibility(View.GONE);


        AMapNaviViewOptions options = new AMapNaviViewOptions();
        options.setTilt(0);
        //
        options.setLayoutVisible(true);
        mAMapNaviView.setViewOptions(options);
    }

    private void initWayPointList() {
//        mWayPointList.add(new NaviLatLng(30.645568, 104.030847));
        mWayPointList.add(new NaviLatLng(30.646119, 104.039657));
//        mWayPointList.add(new NaviLatLng(30.634858, 104.051181));
//        mWayPointList.add(new NaviLatLng(30.63339, 104.061504));
//        mWayPointList.add(new NaviLatLng(30.641845, 104.091124));
//        mWayPointList.add(new NaviLatLng(30.646541, 104.094016));
//        mWayPointList.add(new NaviLatLng(30.659383, 104.101951));
//        mWayPointList.add(new NaviLatLng(30.666385, 104.112218));
    }

    @Override
    public void onInitNaviSuccess() {
        super.onInitNaviSuccess();
        /**
         * 方法: int strategy=mAMapNavi.strategyConvert(congestion, avoidhightspeed, cost, hightspeed, multipleroute); 参数:
         *
         * @congestion 躲避拥堵
         * @avoidhightspeed 不走高速
         * @cost 避免收费
         * @hightspeed 高速优先
         * @multipleroute 多路径
         *
         *  说明: 以上参数都是boolean类型，其中multipleroute参数表示是否多条路线，如果为true则此策略会算出多条路线。
         *  注意: 不走高速与高速优先不能同时为true 高速优先与避免收费不能同时为true
         */
        int strategy = 0;
        try {
            //再次强调，最后一个参数为true时代表多路径，否则代表单路径
            strategy = mAMapNavi.strategyConvert(true, false, false, false, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAMapNavi.calculateDriveRoute(sList, eList, mWayPointList, strategy);

    }

    @Override
    public void onCalculateRouteSuccess(AMapCalcRouteResult aMapCalcRouteResult) {
        super.onCalculateRouteSuccess(aMapCalcRouteResult);
        mAMapNavi.startNavi(NaviType.EMULATOR);
    }

    private long myTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (System.currentTimeMillis() - myTime > 2000) {
                Toast.makeText(SingleRouteCalculateActivity.this,"再按一次退出导航",Toast.LENGTH_SHORT).show();
                myTime = System.currentTimeMillis();
            } else {
                finish();
                return false;
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
