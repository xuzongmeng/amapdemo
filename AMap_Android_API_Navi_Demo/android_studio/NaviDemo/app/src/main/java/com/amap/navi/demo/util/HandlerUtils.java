package com.amap.navi.demo.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class HandlerUtils {
    private HandlerUtils() {}

    public static final Handler handler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };
}
