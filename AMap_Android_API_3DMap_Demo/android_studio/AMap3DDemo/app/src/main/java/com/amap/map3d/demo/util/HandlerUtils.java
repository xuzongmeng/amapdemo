package com.amap.map3d.demo.util;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * author : xuzongmeng
 * date   : 2019/3/25
 * desc   :
 */
public class HandlerUtils {
    private HandlerUtils() {}

    public static final Handler handler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };
}
